# 容器用法速记

![继承结构](attachments/Container.png)

为了方便背诵，可以由上至下进行背诵。
## Iterable

提供`for each` 功能。可`iterator()`返回迭代器。

迭代器提供：`boolean hasNext()`，`E next()` 和 `remove()` （移除刚`next()`返回的元素，只能调用一次，如果刚刚修改了容器，此方法无定义行为。

通过`forEachRemaining(Consumer<? super E> action)` 传入 lambda表达式处理所有元素。

## Collection

实用方法集合：

| 方法 | 说明 |
| --- | --- |
| `int	size()`| 返回容纳元素数量 |
| `boolean	isEmpty()` | 判空 |
| `boolean	add(E e)` | 增加元素 |
| `boolean	addAll(Collection<? extends E> c)` | 批量增加元素 |
| `boolean	remove(Object o)` | 删除元素 |
| `boolean	removeAll(Collection<?> c)`| 批量删除元素 |
| `default boolean	removeIf(Predicate<? super E> filter)`| 数据筛选|
| `void	clear()`| 清空容器 |
| `boolean	retainAll(Collection<?> c)` | 批量保留元素 |
| `boolean	contains(Object o)` | 判断元素存在 |
| `boolean	containsAll(Collection<?> c)` | 批量判断元素存在 |
| `boolean equals(Object o)` | 对比两个容器包含内容（值比较）是否相同 |
| `Object[]	toArray()`| 转换为数组 |
| `<T> T[]	toArray(T[] a)`| 转换为数组，可规定数组元素类型|
| `default Stream<E>	stream()`| 返回串行流 |
| `default Stream<E>	parallelStream()`| 返回并行流 |

![Container2](attachments/Container2.png)
### List

实用方法集合：

| 方法 | 说明 |
| --- | --- |
|`void	add(int index, E element)` | 在某个位置添加元素 |
|`boolean	addAll(int index, Collection<? extends E> c)`| 从某个位置开始批量添加元素 |
| `abstract E	get(int index)` | 得到某个位置元素 |
| `int	hashCode()` | 获得List的散列值 |
| `int	indexOf(Object o)`| 依赖`equals()`方法找到和 `o` 相等的最先出现的元素位置 |
| `int	lastIndexOf(Object o))`| 同上，找到最后出现的元素位置 |
| `ListIterator<E>	listIterator()` | 返回 `ListIterator` 新增了`hasPrevious()` `previous()` `nextIndex()` `previousIndex()` `set()` 功能 |
| `ListIterator<E>	listIterator(int index)` | 从某个位置开始返回 `ListIterator`|
| `E	set(int index, E element)`| 设置某个位置的元素 |
| `List<E>	subList(int fromIndex, int toIndex)`| 获得子List，左闭右开|


#### ArrayList

实用方法集合：

| 方法 | 说明 |
| --- | --- |
| `Object	clone()` | 浅复制 |
| `void	ensureCapacity(int minCapacity)` | 改变容纳量 |
| `void	sort(Comparator<? super E> c)` | 排序 |
| `void	replaceAll(UnaryOperator<E> operator)` | 根据一元运算符替换 |
| `void	trimToSize()` | 将容纳量缩紧至`size()`| 


#### LinkedList

实用方法集合：


### Queue

| 方法 | 说明 |
| --- | --- |
| `boolean	add(E e)`| 如果容量足够，则添加元素并返回`true`，否则报异常|
| `E	element()` | 返回头部元素，如果空报异常 |
| `boolean	offer(E e)` | 和 `add()` 一致 |
| `E	peek()` | 返回头部元素，如果空返回`null`|
| `E	poll()` | 弹出头部元素，如果空返回`null`|
| `E	remove()` | 弹出头部元素，如果空报异常 |

#### Deque

![Deque](attachments/Deque.png)

| 方法 | 说明 |
| --- | --- |
| `E	pop()` | `removeFirst()` |
| `void	push(E e)` | `addFirst()` |
| `boolean removeFirstOccurrence(Object o)` | 值检索，检索到了就删除，成功true|
| `	removeLastOccurrence(Object o)` | 同上，从后往前 |
### Set

#### HashSet

散列集合（HashSet）使用链表数组实现，每个位置一个链表（桶）。桶满时则成为平衡二叉树，在出现大量冲突时提升性能。


| 方法 | 说明 |
| --- | --- |
| boolean	contains(Object o) | 判断包含 |
| boolean	remove(Object o) | 删除 |

#### TreeSet

数集（TreeSet）是一个红黑树。

| 方法 | 说明 |
| --- | --- |
| E	first()| 返回最小值|
| E	last() | 最大值|
| E	ceiling(E e) | 大于等于的值 |
| E	floor(E e) |  小于等于的值|
| E	higher(E e) | 大于的值 |
| E	lower(E e) | 小于的值 |
| E	pollFirst() | 拿出最小值 |
| E	pollLast() | 拿出最大值 |

### Queue


## Map

实用方法集合：

| 方法 | 说明 |
| --- | --- |
| `int	size()` | 所含键值对数量 |
| `boolean	isEmpty()` | 判空 |
| `V	put(K key, V value)` | 添加一个键值对，如果Key已经存在，则替换Value|
| `void	putAll(Map<? extends K,? extends V> m)` | 添加一批键值对 |
| `default V	putIfAbsent(K key, V value)`| 如果这个Key尚没有一个Value（或`null`），则放入成功返回`null`,否则返回Value |
| `void	clear()` | 清空 |
| `V	remove(Object key)` | 按Key删除 |
| `default boolean	remove(Object key, Object value)` | 按键值对删除 |
| `default V	replace(K key, V value)` | 改变指定键的值，只有当此Key存在Value时才替换 |
| `default boolean	replace(K key, V oldValue, V newValue)`| 改变指定键值对的值 |
| `default void	replaceAll(BiFunction<? super K,? super V,? extends V> function)` | 批量改变键值对的值 |
| `V	get(Object key)` | 按Key查Value | 
| `int	hashCode()` | 返回Map的哈希值 |
| `default V	getOrDefault(Object key, V defaultValue` | 同上，查不到则返回默认值 |
| `boolean	containsKey(Object key)`| 检索是否包含Key |
| `boolean	containsValue(Object value)`| 检索是否包含Value |
| `default void	forEach(BiConsumer<? super K,? super V> action)` | 施加 lambda 函数 |
| `boolean	equals(Object o)` | 相当于`m1.entrySet().equals(m2.entrySet())` |
| `Set<Map.Entry<K,V>>	entrySet()` | 将所有键值对变为一个Set |
| `Set<K>	keySet()`| 将所有键变成一个Set |
| `Collection<V>	values()`| 将所有值变成一个Collection |
| `default V	merge(K key, V value, BiFunction<? super V,? super V,? extends V> remappingFunction)` | |
| `default V	compute(K key, BiFunction<? super K,? super V,? extends V> remappingFunction)` ||
| `其他compute方法` | |


### HashMap

散列映射（HashMap）根据键值创建哈希表。

实用方法和Map接口一致。

### TreeMap

树映射（TreeMap）根据键值创建红黑树。
