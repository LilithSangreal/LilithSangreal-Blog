# Python 源码分析笔记

## 参考：

- [cpython 3.7](https://github.com/python/cpython/tree/3.7)
- [python3-source-code-analysis](https://github.com/flaggo/python3-source-code-analysis)

## 一切皆对象

![object-category](attachments/object-category.jpg)

在Python语言中，一切都是对象：


- Fundamental 对象: 类型对象
- Numeric 对象: 数值对象
- Sequence 对象: 容纳其他对象的序列集合对象
- Mapping 对象: 类似 C++中的 map 的关联对象
- Internal 对象: Python 虚拟机在运行时内部使用的对象

### 基础对象 `PyObject` 和 `PyVarObject`

Python的对象分配在堆上，使用垃圾回收机制来进行回收。

Python对象遵循两个原则：

- 对象不能静态分配或分配在栈上
- 对象只能通过特殊的宏或函数来访问

除了类型对象（type objects）不遵循第一个原则：基础类型对象表示为一系列静态初始化的类型对象。

每个对象都拥有引用计数。当引用计数为 0 则回收。

为了能用一个指针来引用一个对象，一个对象一旦被分配在堆上，就有了固定的大小和地址。同类型的对象不一定拥有同样的大小，但一旦被分配了，大小就不会在改变了。（因为一旦移动对象或改变这个对象的大小，就要修改引用这个对象的指针，甚至还要修改其内存上相邻的对象指针）

所有的对象都是通过一个`PyObject *`类型的指针来访问的。它是一个结构体，只包含引用计数和类型指针（这个指针指向一个类型对象，用来决定对象的类型）。

![object-category](attachments/PyObject.jpg)


```Cpp
#define _PyObject_HEAD_EXTRA            \
    struct _object *_ob_next;           \
    struct _object *_ob_prev;

typedef struct _object {
    _PyObject_HEAD_EXTRA
    Py_ssize_t ob_refcnt;
    struct _typeobject *ob_type;
} PyObject;
```

可见，所有对象都被串在一个双向链表上。

显然只有引用技术和指向类型对象的一个结构体，没有办法真正作为一个对象来使用。

事实上，没有任何一个对象是直接作为一个`PyObject`来声明的。但是任何一个指向一个Python对象的指针，都可以转换为一个 `PyObject *`。

实际上，它是作为所有的Python对象的起始段（initial segment）来使用的。所有真正被分配内存的对象开头必须包含这个起始段。因此Python源码中定义了宏。

```Cpp
/* PyObject_HEAD defines the initial segment of every PyObject. */
#define PyObject_HEAD                   PyObject ob_base;
```

基于这个规则，对于Python中存在的变长对象，Python 对这个起始段进行进一步的定制。

```Cpp
typedef struct {
    PyObject ob_base;
    Py_ssize_t ob_size; /* Number of items in variable part */
} PyVarObject;
```

增加了一个特殊的值 `ob_size` 来表示这个对象包含的成员数量（只代表其中包含元素的数量，并非字节数）。

同理，所有的可变长的对象指针都可以转化为一个`PyVarObject *`。但同样，没有任何一个对象是直接作为一个`PyVarObject`来声明的。这个结构仅仅是作为起始段来使用的。

定义宏为：

```Cpp
/* PyObject_VAR_HEAD defines the initial segment of all variable-size
 * container objects.  These end with a declaration of an array with 1
 * element, but enough space is malloc'ed so that the array actually
 * has room for ob_size elements.  Note that ob_size is an element count,
 * not necessarily a byte count.
 */
#define PyObject_VAR_HEAD      PyVarObject ob_base;
```

### 类型对象 `PyTypeObject`

前文提到，起始段中必然包含一个指向类型对象结构体的指针`struct _typeobject *ob_type`。用于表示一个对象的类型。它包含着某个类型对象的全部元信息：名字、分配内存大小等等。

它也是一个Python对象，所以同样遵循着上述原则。

首先，它必须包含起始段：

```Cpp
typedef struct _typeobject {
    PyObject_VAR_HEAD
    ...
}
```

根据起始段可见，类型对象是一个变长对象。

```Cpp

typedef struct _typeobject {
    ...
    const char *tp_name; /* For printing, in format "<module>.<name>" */
    Py_ssize_t tp_basicsize, tp_itemsize; /* For allocation */

    ...
}

```

还包含了对象的名称字符串，和用于分配内存的空间大小变量。

除此之外，还定义了Python对象标准的方法集合。

那么既然类型对象也是一个对象，也包含了指向类型对象的指针。类型对象的类型如下：

```Cpp
PyTypeObject PyType_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    "type",                                     /* tp_name */
    sizeof(PyHeapTypeObject),                   /* tp_basicsize */
    sizeof(PyMemberDef),                        /* tp_itemsize */

    ......
};
```

所有用户自定义的Python类所对应的 PyTypeObject 对象都是通过 `PyType_Type` 创建的。

以一个内建的`int`(long)类型来说明：

```Cpp
PyTypeObject PyLong_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    "int",                                      /* tp_name */
    offsetof(PyLongObject, ob_digit),           /* tp_basicsize */
    sizeof(digit),                              /* tp_itemsize */

    ......
};
```

值得一提的是，其中对起始段的初始化使用了一个宏`PyVarObject_HEAD_INIT(&PyType_Type, 0)`，其定义如下:


```Cpp
#ifdef Py_TRACE_REFS
    #define _PyObject_EXTRA_INIT 0, 0,
#else
    #define _PyObject_EXTRA_INIT
#endif

#define PyObject_HEAD_INIT(type)        \
    { _PyObject_EXTRA_INIT              \
    1, type },
```

当我们在Python中新建一个常数变量`int(10)`时，其运行时如下：

![runtime](attachments/object-runtime-relation.jpg)
