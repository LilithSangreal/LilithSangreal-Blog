# ConvLab介绍及使用
## 目录
<!-- TOC -->

- [ConvLab介绍及使用](#ConvLab%E4%BB%8B%E7%BB%8D%E5%8F%8A%E4%BD%BF%E7%94%A8)
  - [目录](#%E7%9B%AE%E5%BD%95)
  - [论文及代码](#%E8%AE%BA%E6%96%87%E5%8F%8A%E4%BB%A3%E7%A0%81)
  - [简介](#%E7%AE%80%E4%BB%8B)
  - [对话系统架构介绍](#%E5%AF%B9%E8%AF%9D%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84%E4%BB%8B%E7%BB%8D)
  - [ConvLab架构设计](#ConvLab%E6%9E%B6%E6%9E%84%E8%AE%BE%E8%AE%A1)
    - [Agents-Environments-Bodies（AEB）](#Agents-Environments-BodiesAEB)
    - [Session, Trial and Experiment](#Session-Trial-and-Experiment)
  - [使用](#%E4%BD%BF%E7%94%A8)
    - [安装](#%E5%AE%89%E8%A3%85)
      - [Pip](#Pip)
      - [Docker](#Docker)
    - [测试运行](#%E6%B5%8B%E8%AF%95%E8%BF%90%E8%A1%8C)
    - [spec file](#spec-file)

<!-- /TOC -->
## 论文及代码

[论文：ConvLab: Multi-Domain End-to-End Dialog System Platform](https://arxiv.org/abs/1904.08637)

[Github](https://github.com/ConvLab/ConvLab)

## 简介

ConvLab是微软美国研究院和清华联合推出了一款开源的多领域端到端对话系统平台，它包括一系列的可复用组件，比如传统的管道系统（pipline systems：包括多个独立步骤的对话系统）或者端对端的神经元模型。方便研究者可以快速使用这些可复用的组件搭建实验模型。同时，ConvLab还提供了一批标注好的数据集和用这些数据集训练好的的预训练模型。

- ConvLab是第一个开源的、多领域的端对端对话系系统平台。包含了广泛的可训练的统计模型及标注数据。
- ConvLab提供了大量的工具用于创建各种对话系统。研究人员可以在同一环境下横向对比不同方法的效果。
- ConvLab提供了对端到端模型的人类评测（Amazon Mechanical Turk）和模拟评测。

## 对话系统架构介绍

任务导向型对话系统，它的目标是为用户完成特定的任务。

一个完全模块化的对话系统一般分为四个独立步骤：

- 自然语言理解（natural language understanding）：它将用户输入解析为预定义的语义槽。 
- 对话状态跟踪（dialog state tracker）：对话状态跟踪是确保对话系统健壮性的核心组件。它在对话的每一轮次对用户的目标进行预估，管理每个回合的输入和对话历史，输出当前对话状态。
- 对话策略学习（dialog policy）：根据状态跟踪器的状态表示，策略学习是生成下一个可用的系统操作。
- 自然语言生成（natural language generation）：它将选择操作进行映射并生成回复。 

一个完全的端到端系统（end-to-end）则将这些步骤完全地包含在神经网络内部。

若同时使用管道系统和端到端系统两种思想，将会产生多种架构（每一行为一种架构）：

![对话系统架构](attachments/conv_arch.png)

对于以上的所有架构，ConvLab都可以进行快速建模。

## ConvLab架构设计

### Agents-Environments-Bodies（AEB）

为了灵活地实现各种**多领域**对话系统，ConvLab提出了一种Agents-Environments-Bodies（AEB）架构设计。

- Agent：一个对话主体的实例。
- Environment：一个用户模拟器（模拟评测）或人类评测组件（人类评测）实例。
- Body：它是一个特定环境中特定主体的化身，包含了对应着某一组Agent和Environment专用的一个数据集。（在下图中，表示为箭头）

![AEB](attachments/AEB.png)

有了这种架构，我们就能不拘泥于单主体单环境的设定，而是向着更广泛的高级实验启航：

- 多主体学习：集中化主体将所有领域的联合观测值映射到一个联合的行为上。这种方法有一个明显的缺陷，观测空间和行为空间是随着领域指数增长的。为了解决这个问题，可以把一个集中化的主体拆分为多个主体（包括分层强化学习主体：利用多个Agent共同协作学习，达到并行处理的效果，减少了学习时间，加快寻找最优策略的速度）。在上图中，旅行问答就可以拆分为饭店问答和旅店问答。
- 多任务学习：每个对话主体其实可以从不同的领域学习到一些通用知识。这也就是说，每个对话主体在任何一个环境中都可以存在一个Body。如图所示，每个主体都可以从所有的环境里学习知识。
- 角色扮演：可以利用AEB方便地设计一个角色扮演系统：一个主体扮演系统，另一个主体扮演用户，通过循环环境彼此交谈。

### Session, Trial and Experiment

为了系统地比较主体和环境以实现自动化调参。ConvLab使用了[SLM Lab](https://kengz.gitbooks.io/slm-lab/content/)和 Ray 进行层级控制。也就是说每一层都是Session, Trial和Experiment的形式并产生相应的评估报告。

- Session：每个Session会初始化一组主体和一组环境，并运行预定好数量的一组数据。
- Trial：每个Trial都会保存一个固定集合的参数值，并用一个随机数种子跑多个Session。然后分析这些Session取平均值
- Experiment：一个experiment的输入是一组超参数，输出则是用针对任务本身定义的数组来评价（比如成功率和平均奖赏），最终目的就是找到一组使模型表现最好的超参数。

## 使用

### 安装

ConvLab只支持比Python3.6.5更新的版本，不支持Windows系统。

#### Pip

- 确保你的Python版本足够新，更新或使用虚拟环境软件。确保系统中安装了`build-essential`。
- `clone`项目到本地：

```Shell
git clone https://github.com/ConvLab/ConvLab.git
```

- 进入项目文件夹并使用pip安装依赖

```Shell
pip install -r requirements.txt
```

- 下载stopwords

```Shell
python -m nltk.downloader stopwords
```

#### Docker

- 拉取镜像

```Shell
docker pull convlab/convlab:0.2
```

- 运行镜像

```Shell
docker run -it --rm convlab/convlab:0.2
```

### 测试运行

运行ConvLab将要使用以下命令：

```Shell
python run.py {spec file} {spec name} {mode}
```

ConvLab项目提供了例程测试（同时可以配置log细节等级），其配置文件`spec file`为：[demo.json](https://github.com/ConvLab/ConvLab/blob/master/convlab/spec/demo.json)（注：此文件包含了多个训练配置，由`spec name`定义）

```Shell
# 评测如下对话系统
# NLU：OneNet (Kim et al., 2017)
# DST：基于规则
# Policy：基于规则
# NLG：基于模板

$ python run.py demo.json onenet_rule_rule_template eval

# to see natural language utterances 
$ LOG_LEVEL=NL python run.py demo.json onenet_rule_rule_template eval

# to see natural language utterances and dialog acts 
$ LOG_LEVEL=ACT python run.py demo.json onenet_rule_rule_template eval

# to see natural language utterances, dialog acts and state representation
$ LOG_LEVEL=STATE python run.py demo.json onenet_rule_rule_template eval
```

### spec file

正如[demo.json](https://github.com/ConvLab/ConvLab/blob/master/convlab/spec/demo.json)所示。ConvLab使用了非常方便的JSON来配置模型，而不需要编写大量代码。其基于[SLM Lab，请参考文档](https://kengz.gitbooks.io/slm-lab/content/)。