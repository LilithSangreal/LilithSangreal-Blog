# 基础知识篇：数据库知识汇总

## MySQL 安装及启动配置

```Shell
# 安装
sudo apt install mysql-service mysql-client
# 启动
sudo service mysql [start/stop/status]
# 默认帐密 适用ubuntu18
sudo cat /etc/mysql/debian.cnf
# 修改密码
mysql> set password for user@localhost = password('password')
```

## MySQL 用户增删及权限配置
```sql
# 0. 使用mysql库
use mysql
# 1. 创建用户:
# 指定ip：192.118.1.1的mjj用户登录
create user 'alex'@'192.118.1.1' identified by 'password';
# 指定ip：192.118.1.开头的mjj用户登录
create user 'alex'@'192.118.1.%' identified by 'password';
# 指定任何ip的mjj用户登录
create user 'alex'@'%' identified by 'password';

# 2. 删除用户
drop user '用户名'@'IP地址';


# 3. 修改用户
rename user '用户名'@'IP地址' to '新用户名'@'IP地址';

# 4. 修改密码
set password for '用户名'@'IP地址'=Password('新密码');
```

## MySQL 授权

```sql
# 查看权限
show grants for '用户'@'IP地址'

# 授权 mjj用户仅对db1.t1文件有查询、插入和更新的操作
grant select ,insert,update on db1.t1 to "alex"@'%';

# 表示有所有的权限，除了grant这个命令，这个命令是root才有的。mjj用户对db1下的t1文件有任意操作
grant all privileges  on db1.t1 to "alex"@'%';
#mjj用户对db1数据库中的文件执行任何操作
grant all privileges  on db1.* to "alex"@'%';
#mjj用户对所有数据库中文件有任何操作
grant all privileges  on *.*  to "alex"@'%';

# 取消权限

# 取消mjj用户对db1的t1文件的任意操作
revoke all on db1.t1 from 'alex'@"%";  

# 取消来自远程服务器的mjj用户对数据库db1的所有表的所有权限

revoke all on db1.* from 'alex'@"%";  

# 取消来自远程服务器的mjj用户所有数据库的所有的表的权限
revoke all privileges on *.* from 'alex'@'%';
```

## MySQL 备份

```sql
# 备份：数据表结构+数据
mysqdump -u root db1 > db1.sql -p


# 备份：数据表结构
mysqdump -u root -d db1 > db1.sql -p

# 导入现有的数据到某个数据库
# 1.先创建一个新的数据库
create database db10;
# 2.将已有的数据库文件导入到db10数据库中
mysqdump -u root -d db10 < db1.sql -p
```
## B 树，B+ 树

## ACID

ACID 分别由事务管理、开发人员、并发控制、恢复管理部件确保。

### 原子性 Atomicity

一个事务（transaction）是一个不可分割的工作单位。一个事务中的所有操作，要么全部完成，要么全部不完成，不会结束在中间某个环节。如果事务执行中发生错误，会回滚（Rollback）到事务开始前的状态，就像这个事务从来没有执行过一样。

### 一致性 Consistency

在事务开始之前和事务结束以后，数据库的完整性没有被破坏。这表示写入的资料必须完全符合所有的预设规则，这包含资料的精确度、串联性以及后续数据库可以自发性地完成预定的工作。

### 隔离性 Isolation

数据库允许多个并发事务同时对其数据进行读写和修改的能力，隔离性可以防止多个事务并发执行时由于交叉执行而导致数据的不一致。事务隔离分为不同级别，包括读未提交（Read uncommitted）、读提交（read committed）、可重复读（repeatable read）和串行化（Serializable）。

### 持久性 Durability

事务处理结束后，对数据的修改就是永久的，即便系统故障也不会丢失。

MySQL以页为单位读取数据，一页里面有