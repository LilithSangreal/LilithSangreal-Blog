# untitled.md

## Encoder 编码器

文件：`fariseq_encoder.py`

一个编码器的抽象基类定义如下：

```Python
class FairseqEncoder(nn.Module):
    """Base class for encoders."""
    def __init__(self, dictionary):
    def forward(self, src_tokens, src_lengths=None, **kwargs):
    def max_positions(self):
    def upgrade_state_dict(self, state_dict):
```

`FairseqEncoder` 是一个Pytorch模块，固定的实例变量是一个保存信息的字典：

```Python
    def __init__(self, dictionary):
        super().__init__()
        self.dictionary = dictionary
```

### 必须实现的方法

- 前向传播

```Python
    def forward(self, src_tokens, src_lengths=None, **kwargs):
        """
        Args:
            src_tokens (LongTensor): 源语言的tokens，形状规定为：
                `(batch, src_len)`
            src_lengths (LongTensor): 一个规定每个源语言的句子的长度的Tensor，形状：
                `(batch)`
        """
        raise NotImplementedError
```

- 重新排序

```Python
    def reorder_encoder_out(self, encoder_out, new_order):
        """
        根据 `new_order` 来对编码器输出进行重排序

        Args:
            encoder_out: ``forward()`` 的输出
            new_order (LongTensor): 希望的顺序

        Returns:
            `encoder_out` 根据 `new_order` 重排序
        """
        raise NotImplementedError
```

### 可选的方法

```Python
    def max_positions(self):
        """Maximum input length supported by the encoder."""
        return 1e6  # an arbitrary large number
```

这个方法规定了编码器最大输入的长度。

```Python
    def upgrade_state_dict(self, state_dict):
        """Upgrade a (possibly old) state dict for new versions of fairseq."""
        return state_dict
```

对于新版本的fairseq，进行状态字典的升级。#TODO

## Decoder 编码器

```Python
    def __init__(self, dictionary):
        super().__init__()
        self.dictionary = dictionary
        self.onnx_trace = False
```

`FairseqDecoder` 是一个Pytorch模块，固定的实例变量是一个保存信息的字典，和`onnx_trace`#TODO。


