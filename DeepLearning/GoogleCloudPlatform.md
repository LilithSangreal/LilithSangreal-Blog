# GCP 使用杂记

### 请为虚拟机和 TPU 资源使用同一名称和地区。

## 创建计算引擎

```Bash
gcloud compute instances create lilithmnist --machine-type=n1-standard-1 --image-project=ml-images --image-family=tf-1-14 --boot-disk-size=10GB --boot-disk-type=pd-ssd --scopes=cloud-platform
```

```Bash
Created [https://www.googleapis.com/compute/v1/projects/nlplearn-260107/zones/asia-east1-c/instances/lilithmnist].
NAME         ZONE          MACHINE_TYPE   PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP     STATUS
lilithmnist  asia-east1-c  n1-standard-1               10.140.0.2   35.236.177.174  RUNNING
```

## 创建TPU

```
gcloud compute tpus create lilithmnist zone=us-central1-b network=default accelerator-type=v2-8 range=192.168.0.0/29 --version=1.14
```

## 运行模型

```Bash
python /usr/share/models/official/mnist/mnist_tpu.py \
  --tpu=lilithmnist \
  --data_dir=lilithmnist/data \
  --model_dir=lilithmnist/output \
  --use_tpu=True \
  
  --iterations=500 \
  --train_steps=2000
```