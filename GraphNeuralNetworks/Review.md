# 图神经网络综述
<!-- TOC -->

- [图神经网络综述](#图神经网络综述)
    - [图神经网络分类和变种](#图神经网络分类和变种)
    - [基础图神经网络原理](#基础图神经网络原理)
        - [局限性](#局限性)
    - [针对图类型分类](#针对图类型分类)
        - [有向图](#有向图)
        - [异构图（Heterogeneous Graphs）](#异构图heterogeneous-graphs)
        - [带有边信息的图](#带有边信息的图)
        - [动态图](#动态图)
    - [传播方式](#传播方式)
        - [](#)

<!-- /TOC -->

## 图神经网络分类和变种

![catagory](attachments/catagory.png)

## 基础图神经网络原理

类似于词嵌入，原始的图神经网络意图学习一个节点嵌入，即训练一个图神经网络来学习一个状态嵌入$\mathbf{h}_{v} \in \mathbb{R}^{s}$（state embedding）。这个状态向量可以包含临界节点的信息。

$$\mathbf{h}_{v}=f\left(\mathbf{x}_{v}, \mathbf{x}_{c o[v]}, \mathbf{h}_{n e[v]}, \mathbf{x}_{n e[v]}\right)$$

如上式，$\mathbf{x}_{v}, \mathbf{x}_{c o}[v], \mathbf{h}_{n e}[v], \mathbf{x}_{n e[v]}$分别代表 $v$ 节点包含的特征，其相连的边上代表的特征，相邻节点的状态嵌入和相邻节点包含的特征。

这个函数称为局部转换函数（local transition function）。此函数的参数是所有节点都共享的。

$$
\mathbf{o}_{v}=g\left(\mathbf{h}_{v}, \mathbf{x}_{v}\right)
$$

这个函数称为局部输出函数（local output function）根据节点状态嵌入并结合当前节点特征，可以得到某个节点的输出。

将特征组成矩阵，变化为如下形式:

$$
\mathbf{H}=F(\mathbf{H}, \mathbf{X})
$$

$$
\mathbf{O}=G\left(\mathbf{H}, \mathbf{X}_{N}\right)
$$

宏观来看，整体的节点状态嵌入除了取决于每个节点自身的特征，还取决于整体的节点状态嵌入本身和整体的节点的特征。输出亦然。

在这里，最原始的GNN做了一个假设，假设 $F$ 函数为一个压缩变换映射。考虑节点特征嵌入空间是一个完备的度量空间（若维度为3，实际上就是欧几里得度量空间）。完备度量空间上的压缩变换映射，必有一个不动点。实际上我们寻找的就是不动点来作为这次迭代的状态嵌入。（巴拿赫不动点定理）

根据夹挤定理，只要我们不断使用这个压缩映射来递归创造一个数列，最终数列极限为不动点。

所以，我们就可以这样就可以迭代足够多次，直到逐渐逼近不动点。

$$
\mathbf{H}^{t+1}=F\left(\mathbf{H}^{t}, \mathbf{X}\right)
$$

$f$ 函数显然可以定义为前向传播的神经网络，那么训练过程梯度下降即可（有监督情况下）:

考虑有监督情况下，我们的目的是最终找出的不动点（目前暂时看作节点的输出）和标记相同（Label）。决定一个压缩变换映射的不动点位置的究竟是什么呢？显然是初始值和映射 $F$ 本身。而 $F$ 由权重 $W$ 决定。这个权重根据梯度下降来更新。

### 局限性

- 为了逼近不动点，迭代的更新节点的隐藏状态非常低效（提出弱化不动点的假设并设计多层GNN来标识节点和邻居，不明白意思）
- GNN在不同的节点共享同样的参数，无法表示出层次信息。此外这种顺序的更新过程可以考虑使用循环神经网络甚至LSTM。
- 很难对边上的信息进行良好的建模（比如，知识图谱的边表示各种关系）

## 针对图类型分类

![图类型](attachments/graph_types.png)

### 有向图

DGP使用两套权重分配表示作为父亲节点的权重和作为子节点的权重（有向边由父亲指向儿子）。其结构如下：

$$
\mathbf{H}^{t}=\sigma\left(\mathbf{D}_{p}^{-1} \mathbf{A}_{p} \sigma\left(\mathbf{D}_{c}^{-1} \mathbf{A}_{c} \mathbf{H}^{t-1} \mathbf{W}_{c}\right) \mathbf{W}_{p}\right)
$$

其中$\mathbf{D}_{p}^{-1} \mathbf{A}_{p}, \mathbf{D}_{c}^{-1} \mathbf{A}_{c}$分别表示父子节点标准化了的邻接矩阵。

### 异构图（Heterogeneous Graphs）

异构图是包含了节点本身可以分为不同种类的节点。（比如红黑树的红节点和黑节点）

- 一种最简单的方法是直接把节点的不同种类进行独热编码，作为一种节点特征使用。
- GraphInception提出了元路径（metapath），使用这个工具可以将节点按照类型和距离进行分组。
- 异构图注意力网络（HAN）使用了节点级别和语义级别的注意力机制。

### 带有边信息的图

- 一种思路是直接将带有边信息的边作为一个节点使用，即一条边拆成两条边，信息节点放中间。如 G2S。
- 另一种思路对于不同的边类型使用不同的权重矩阵进行传播。

### 动态图

如果一个固定的图结构却动有动态的输入信号，那么这是动态图。

![动态图示例](attachments/st_graph.png)

比如上图这个人体骨架节点图，由于人体在不断运动，节点的空间信息随着时间而改变。（spatial temporal graph）

[DCRNN](https://arxiv.org/pdf/1707.01926.pdf) 和 [STGCN](https://arxiv.org/pdf/1709.04875.pdf) 来使用GNN来捕获空间信息。他们将输出信息放入一个序列模型，比如seq2seq模型或CNN。

[Structural-RNN](http://openaccess.thecvf.com/content_cvpr_2016/papers/Jain_Structural-RNN_Deep_Learning_CVPR_2016_paper.pdf) 和 [ST-GCN](https://arxiv.org/pdf/1801.07455.pdf) 可以同时捕获空间信息和时域信息。他们用含有时域信息的连接来拓展了原图，然后使用传统的 GNN 来进行处理。

## 传播方式

![prop](attachments/prop_types.png)

### 卷积

#### 频谱方法

##### 谐波分析在带权图上的推广

考虑傅里叶变换：

$$
f(x)=\frac{a_{0}}{2}+\sum_{n=1}^{\infty}\left(a_{n} \cos n x+b_{n} \sin n x\right)
$$

随着谐波频率的增加，能量逐渐衰减。

如何来定义带权图的频率？考虑频率越高越不光滑，定义图上的光滑度。

$$
\|\nabla x\|_{W}^{2}=\sum_{i} \sum_{j} W_{i j}[x(i)-x(j)]^{2}
$$

则使光滑度最好的向量值为一个常量：

$$
v_{0}=\underset{x \in \mathbb{R}^{m}}{\arg \min }\|\nabla x\|_{W}^{2}=(1 / \sqrt{m}) 1_{m}
$$

接着不断求出仍然能使光滑度尽可能小的向量，并与之前所有的向量正交：

$$
v_{i}=\arg \min _{\|x\|=1} \arg \min _{ | x \|=1} x \perp\left\{v_{0}, \ldots, v_{i-1}\right\}
$$

这些向量排成一队，使得光滑度逐渐下降。若这些向量都是此图拉普拉斯矩阵的特征向量。即通过特征值，可以得到原始拉普拉斯矩阵的一种表示。

于是，谐波分析就推广到了带权图上。

$$
x_{k+1, j}=h\left(V \sum_{i=1}^{f_{k-1}} F_{k, i, j} V^{T} x_{k, i}\right) \quad\left(j=1 \ldots f_{k}\right)
$$

其中

$$
\mathbf{g}_{\theta} \star \mathbf{x}=\mathbf{U} \mathbf{g}_{\theta}(\mathbf{\Lambda}) \mathbf{U}^{T} \mathbf{x}
$$

