# Self-Sizing Table View Cell 实践


estimatedRowHeight 是一个预估高度。
## heightForRowAtIndexPath 可能会被调用的时机

如果没有使能预估高度能力，则会全量调用 heightForRowAtIndexPath 来计算出整个的contentSize。

可能的调用时机：

1. view即将展示在视图上的时候
2. setLayoutMargins
3. layoutSubviews 时
4. 当使用一个regist之后的cell的时候会拉去本行cell 的高度
5. cell 进行了layoutSubviews 时

## Self-Sizing

## Cell height Cache
