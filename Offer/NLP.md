# 自然语言处理方向面试题目汇总

| 面试题 | 公司 | 频率 |
| --- | --- | --- |
| 追问项目模型：搭建，设置超参，训练时间，各种模型对比 | 蚂蚁 | |
| tf-idf | | |
| lstm 为什么比 rnn 效果好？| | |
| word2vector和glove的区别 | 腾讯 | |
| word2vector的实现细节 | 腾讯 | |
| 谈一下attention | 腾讯 | |
| self attention | 蚂蚁  | |
| bert | 腾讯  蚂蚁| 2|
| seq2seq | 蚂蚁 | |
| 对抗学习有了解吗？ | 蚂蚁| |
| graph embedding和random walk | 蚂蚁 |  |
| 以及TextRank算法 | 招银 | |
| pytorch中LSTM如何实现的 | 招银 | |